Realizad por parejas un pequeño programa en Java que realice las siguientes acciones haciendo el control de versiones en vuestro repositorio de GitLab en un subgrupo llamado Repaso dejando a la elección del alumno el nombre del proyecto:

- Declaración e inicialización de un array de enteros de tamaño 10
- Introducción de valores en el array y visualización de los mismos: en cada posición el contenido será su número de posición
- Modificación del array y visualización del mismo: en las posiciones pares del array se modificará el contenido que tenga por 0
- Visualización del número de ceros que hay en el array.
- Visualización del número de elementos distintos de cero que hay en el array.

Se establecerán roles de administrador y colaborador realizando al menos 3 hitos, que se definirán en las tareas que defina el administrador.

Al finalizar el ejercicio se intercambiarán los roles y se repetirá el mismo proceso, modificando en qué puntos se realizan los hitos del proyecto.